package at.jku.hdzb;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
public class HDZConfig {
	
	@Getter
	@Setter
	@Value("${hdz.admin.id}")
	Long adminId;
	

}
