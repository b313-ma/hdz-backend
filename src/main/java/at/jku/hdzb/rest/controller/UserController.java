package at.jku.hdzb.rest.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import at.jku.hdzb.model.User;
import at.jku.hdzb.service.UserService;
import io.swagger.v3.oas.annotations.Parameter;

@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = {"http://localhost:3000", "https://hdz-frontend.herokuapp.com"})
public class UserController {
	
	@Autowired
	UserService service;
	
	@GetMapping("")
	public ResponseEntity<List<User>> findAll() {
		return ResponseEntity.ok(service.findAll());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<User> findById(@PathVariable("id") @Parameter(name = "id") Long id) {
		Optional<User> optUser = service.findById(id);
		if (optUser.isPresent()) {
			return ResponseEntity.ok(optUser.get());
		}
		return ResponseEntity.notFound().build();
	}
}
