package at.jku.hdzb.rest.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.hibernate.id.BulkInsertionCapableIdentifierGenerator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import at.jku.hdzb.model.Delivery;
import at.jku.hdzb.model.Partner;
import at.jku.hdzb.model.enums.EDeliveryState;
import at.jku.hdzb.model.enums.ESyncStatus;
import at.jku.hdzb.rest.dto.CsvShipmentDTO;
import at.jku.hdzb.rest.dto.PartnerDTO;
import at.jku.hdzb.rest.dto.PartnerWithDelivieresDTO;
import at.jku.hdzb.rest.dto.mapper.PartnerDTOMapper;
import at.jku.hdzb.rest.xml.mapper.ShipmentMesMapper;
import at.jku.hdzb.service.DeliveryService;
import at.jku.hdzb.service.PartnerService;
import at.jku.hdzb.service.SyncHistService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class RestShipmentFacade {
	
	@Autowired
	private SyncHistService syncHistService;
	
	@Autowired
	private PartnerService partnerService;
	
	@Autowired
	private DeliveryService deliveryService;
	
	@Autowired
	private PartnerDTOMapper partnerDTOMapper;
	
	@Autowired
	private ShipmentMesMapper shipmentMesMapper;
	
	public ResponseEntity<String> pushShipment(String value) {
		StringBuilder sb = new StringBuilder().append(value);
		
		String orderIdentifier = null;
		try {
			String json = new ObjectMapper().writeValueAsString(new XmlMapper().readValue(sb.toString().getBytes(), List.class));
			JSONArray jo = (JSONArray) new JSONParser().parse(json);
			
			orderIdentifier = shipmentMesMapper.getOrderIdentifier(jo); 
			
			if (syncHistService.findByIdentifierAndAlreadySynced(orderIdentifier).size() > 0) {
				return ResponseEntity.status(HttpStatus.CONFLICT).body("Shipment mit identifier " + orderIdentifier + " wurde bereits verarbeitet.");
			}
			
			PartnerDTO deliveryTo = shipmentMesMapper.getDeliveryToPartner(jo);
			PartnerDTO deliveryFrom = shipmentMesMapper.getDeliveryFromPartner(jo);
			
			//TODO sollte woanders herkommen...
			deliveryTo.setLatitude(BigDecimal.valueOf(1));
			deliveryTo.setLongitude(BigDecimal.valueOf(1));
			
			// TODO hier noch eine Logik überlegen ab wann etwas als Site gilt
			deliveryTo.setIsSite(true);
			
			Partner partnerTo = partnerDTOMapper.toEntity(deliveryTo);
			Partner partnerFrom = partnerDTOMapper.toEntity(deliveryFrom);
			Delivery d = shipmentMesMapper.getDelivery(jo);
			
			createDelivery(partnerFrom, partnerTo, d);
			
			syncHistService.setSyncedWith(orderIdentifier, ESyncStatus.S);
			
		} catch (StreamReadException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (DatabindException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ResponseEntity.ok("Success: " + orderIdentifier);
	}
	
	@Transactional
	private void createDelivery(Partner partnerFrom, Partner partnerTo, Delivery delivery) {
		partnerTo = partnerService.saveOrUpdate(partnerTo);
		partnerFrom = partnerService.saveOrUpdate(partnerFrom);
		
		delivery.setTo(partnerTo);
		delivery.setFrom(partnerFrom);
		
		deliveryService.save(delivery);
	}

	public String pushShipmentFromCsv(List<CsvShipmentDTO> shipments) {
		for (CsvShipmentDTO s : shipments) {
			Partner partnerFrom = Partner.builder()
					.identifier(Long.valueOf(s.getBenutzergruppe().concat(s.getFromPartnerLadestelle()).hashCode()))
					.name(s.getBenutzergruppe().concat(" - ").concat(s.getFromPartnerLadestelle()))
					.city(s.getFromPartnerAddress())
					.build();
			
			Partner partnerTo = Partner.builder()
					.identifier(Long.valueOf(s.getToPartnerName().concat(s.getToPartnerAddress()).hashCode()))
					.name(s.getToPartnerName())
					.city(s.getToPartnerAddress())
					.isSite(true)
					.build();
			LocalDateTime deliveryDt;
			
			try {
				deliveryDt = LocalDateTime.parse(s.getLadedatum(), DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
			} catch (Exception e) {
				log.error("Error parsing date: " + s.getLadedatum() + " from csv sendungssnummer: " + s.getSendungsnummer());
				deliveryDt= LocalDateTime.now();
			}
			
			Integer amount = 0;
			
			try {
				 amount = Integer.valueOf(s.getCargospaceAmount());
			} catch (NumberFormatException e) {
				log.error("Error parsing amount: " + s.getCargospaceAmount() + " from csv sendungssnummer: " + s.getSendungsnummer());
				amount = ThreadLocalRandom.current().nextInt(1,100 + 1);
			}
			
			Delivery d = Delivery.builder()
					.cargoSpaceAmount(amount)
					.cargoSpaceTypeCode(s.getCargospaceTypeCode())
					.deliveryDateTime(deliveryDt)
					.state(EDeliveryState.O)
					.build();
			
			createDelivery(partnerFrom, partnerTo, d);
					
		}
		
		return "done";

		
	}

}
