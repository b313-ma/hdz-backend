package at.jku.hdzb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import at.jku.hdzb.model.Partner;
import at.jku.hdzb.rest.dto.PartnerDTO;
import at.jku.hdzb.rest.dto.PartnerWithDelivieresDTO;
import at.jku.hdzb.rest.dto.mapper.PartnerDTOMapper;
import at.jku.hdzb.service.PartnerService;

@Component
public class RestPartnerFacade {

	@Autowired
	private PartnerService partnerService;
	
	@Autowired
	private PartnerDTOMapper partnerDTOMapper;
	
	public ResponseEntity<List<PartnerDTO>> getAllWithMinimalDTOfor(Long userId) {
		return ResponseEntity.ok(partnerService.findAllMinimalDTOfor(userId));
	}

	public ResponseEntity<PartnerWithDelivieresDTO> getDetailsByIdForUser(Long id, Long userId) {
		Partner s = partnerService.findById(id);
		return ResponseEntity.ok(partnerDTOMapper.mapToPartnerWithDeliveriesDTOForUser(s, userId));
	}
	
	

}
