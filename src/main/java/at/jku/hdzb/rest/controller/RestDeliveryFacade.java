package at.jku.hdzb.rest.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

import at.jku.hdzb.model.Delivery;
import at.jku.hdzb.model.enums.EDeliveryState;
import at.jku.hdzb.rest.dto.DeliveryDTO;
import at.jku.hdzb.rest.dto.mapper.DeliveryDTOMapper;
import at.jku.hdzb.service.DeliveryService;
import at.jku.hdzb.service.UserService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class RestDeliveryFacade {
	
	@Autowired 
	DeliveryService deliveryService;
	
	@Autowired
	private DeliveryDTOMapper deliveryMapper;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ObjectMapper objectMapper = new ObjectMapper();
	
	public ResponseEntity<DeliveryDTO> patchDelivery(Long id, JsonPatch patch) {
		try {
			Delivery d = deliveryService.findById(id).orElseThrow(() -> new EntityNotFoundException("No delivery found for id " + id));
			DeliveryDTO dPatched = applyPatchToDelivery(patch, DeliveryDTOMapper.mapToDeliveryDTO(d));
			Delivery patchedEntity = deliveryMapper.mapToEntity(dPatched);
			
			if (patchedEntity.getState().equals(EDeliveryState.R)) {
				// TODO hier können Aktionen gesetzt werden, wie beispielsweise eine eMail senden
				d.setDeliveryReturnRequestedDateTime(LocalDateTime.now());
				log.debug("http://localhost:3000/itemrequested/" + d.getId());
				log.debug("E-Mail an Partner senden noch implementieren bzw link generieren");
			}
			if (patchedEntity.getState().equals(EDeliveryState.V)) {
				d.setDeliveryAufschubRequestedDateTime(LocalDateTime.now());
				log.debug("Aufschub für Delivery " + d.getId() +  " wurde angefragt.");
				log.debug("E-Mail an Partner senden noch implementieren bzw link generieren");
			}
			
			deliveryService.updateStatus(patchedEntity);
						

			
			return ResponseEntity.ok(dPatched);
		} catch (JsonPatchException | JsonProcessingException e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (EntityNotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		
	}
	
	private DeliveryDTO applyPatchToDelivery(
			  JsonPatch patch, DeliveryDTO targetDelivery) throws JsonPatchException, JsonProcessingException {
			    JsonNode patched = patch.apply(objectMapper.convertValue(targetDelivery, JsonNode.class));
			    return objectMapper.treeToValue(patched, DeliveryDTO.class);
			}

	public ResponseEntity<DeliveryDTO> findById(Long id) {
		return ResponseEntity.ok(DeliveryDTOMapper.mapToDeliveryDTO(deliveryService.findById(id).orElseThrow(() -> new EntityNotFoundException("No delivery found for ID " + id))));
	}

	public ResponseEntity<List<DeliveryDTO>> findAll(Long userId) {
		List<DeliveryDTO> dtos;
		if (userService.isAdmin(userId)) {
			dtos = deliveryService.findAll().stream().map(DeliveryDTOMapper::mapToDeliveryDTO).collect(Collectors.toList());
		} else {
			dtos = deliveryService.findAllForUser(userId).stream().map(DeliveryDTOMapper::mapToDeliveryDTO).collect(Collectors.toList());
		}
		
		if (dtos.size() > 0) {
			return ResponseEntity.ok(dtos);
		} else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}
	}

}
