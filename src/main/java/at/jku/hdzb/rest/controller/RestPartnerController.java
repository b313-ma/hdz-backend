package at.jku.hdzb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import at.jku.hdzb.rest.dto.PartnerDTO;
import at.jku.hdzb.rest.dto.PartnerWithDelivieresDTO;
import io.swagger.v3.oas.annotations.Parameter;

@RestController
@RequestMapping("/api/partner")
@CrossOrigin(origins = {"http://localhost:3000", "https://hdz-frontend.herokuapp.com"})
public class RestPartnerController {
//TODO https://www.baeldung.com/spring-rest-openapi-documentation#exposing-pagination-information
	
	@Autowired
	RestPartnerFacade restPartnerFacade;
	
	@GetMapping()
	public ResponseEntity<List<PartnerDTO>> getAll(@RequestHeader(required = true, name = "userId") Long userId) {
		return restPartnerFacade.getAllWithMinimalDTOfor(userId);
	};
	
	@GetMapping("{id}")
	public ResponseEntity<PartnerWithDelivieresDTO> getDetailsById(
			@RequestHeader(required = true, name = "userId") Long userId, 
			@PathVariable("id") @Parameter(name = "id") Long id) {
		return restPartnerFacade.getDetailsByIdForUser(id, userId);
	};
}
