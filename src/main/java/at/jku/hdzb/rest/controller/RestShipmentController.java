package at.jku.hdzb.rest.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import at.jku.hdzb.rest.dto.CsvShipmentDTO;

@RestController
@RequestMapping("/api/shipment")
@CrossOrigin(origins = {"http://localhost:3000", "https://hdz-frontend.herokuapp.com"})
public class RestShipmentController {
	
	@Autowired
	private RestShipmentFacade restShipmentFacade;
	
	@PostMapping(consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> pushShipment(@RequestBody String value) {
		return restShipmentFacade.pushShipment(value);
	}	
	
	@PostMapping(path = "/csv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String uploadCSVFile(@RequestParam("file") MultipartFile file) {
		if (file.isEmpty()) {
			return "file is empty";
		}
		
		try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
			CsvToBean<CsvShipmentDTO> csvToBean = new CsvToBeanBuilder<CsvShipmentDTO>(reader)
					.withType(CsvShipmentDTO.class)
					.withIgnoreLeadingWhiteSpace(true)
					.withSeparator(';')
					.build();
			
			List<CsvShipmentDTO> shipments = csvToBean.parse();
			
			return restShipmentFacade.pushShipmentFromCsv(shipments);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "error";

	}

}
