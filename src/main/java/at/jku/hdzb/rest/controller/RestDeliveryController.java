package at.jku.hdzb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.fge.jsonpatch.JsonPatch;

import at.jku.hdzb.rest.dto.DeliveryDTO;
import io.swagger.v3.oas.annotations.Parameter;

@RestController
@RequestMapping("/api/delivery")
@CrossOrigin(origins = {"http://localhost:3000", "https://hdz-frontend.herokuapp.com"})
public class RestDeliveryController {
	
	@Autowired
	private RestDeliveryFacade restDeliveryFacade;
	
	@PatchMapping(path = "/{id}", consumes = "application/json")
	public ResponseEntity<DeliveryDTO> patchDelivery(@PathVariable Long id, @RequestBody JsonPatch patch) {
		return restDeliveryFacade.patchDelivery(id, patch);
	}
	
	@GetMapping("{id}")
	public ResponseEntity<DeliveryDTO> getDeliveryById(@PathVariable("id") @Parameter(name ="id") Long id)  {
		return restDeliveryFacade.findById(id);
	}
	
	@GetMapping()
	public ResponseEntity<List<DeliveryDTO>> getDeliveries(@RequestHeader(required = true, name = "userId") Long userId) {
		return restDeliveryFacade.findAll(userId);
	}

}
