package at.jku.hdzb.rest.xml.mapper;

import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@Component
public class OrderMesMapper {

	public String getOrderIdentifier(Document doc) {
		Node orderMesNode = doc.getElementsByTagName("ORDERMes").item(0);
		NodeList orderMes = orderMesNode.getChildNodes();
		for (int i = 0; i < orderMes.getLength(); i++) {
			Node n = orderMes.item(i);
			if(n.getNodeType() == Node.ELEMENT_NODE && n.getNodeName().equals("header")) {
				NodeList header = n.getChildNodes();
				for(int j = 0; j < header.getLength(); j++) {
					Node nChild = header.item(j);
					if(nChild.getNodeType() == Node.ELEMENT_NODE && nChild.getNodeName().equals("identifier")) {
						return nChild.getNodeValue();
					}
				}
			}
		}
		
		
		return null;
	}

}
