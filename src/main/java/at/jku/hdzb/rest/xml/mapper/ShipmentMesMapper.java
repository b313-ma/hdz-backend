package at.jku.hdzb.rest.xml.mapper;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import at.jku.hdzb.model.Delivery;
import at.jku.hdzb.model.enums.EDeliveryState;
import at.jku.hdzb.rest.dto.PartnerDTO;
import at.jku.hdzb.rest.dto.mapper.PartnerDTOMapper;

@Component
public class ShipmentMesMapper {
	
	private final int INDEX_OF_HEADER = 0;
	private final int INDEX_OF_DATA = 1;
	private final String KEY_IDENTIFIER = "identifier";
	private final String DTC_DELIVERY = "DE";
	private final String PTC_DELIVERY_ADDRESS = "DA";
	private final String PTC_PICKUP_ADDRESS = "PA";
	
	private final DateTimeFormatter DEF_DTFORMAT = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
	
	@Autowired
	PartnerDTOMapper partnerDTOMapper;
	

	public PartnerDTO getDeliveryToPartner(JSONArray jo) {
		return getDeliveryPartner(jo, PTC_DELIVERY_ADDRESS);
	}

	public String getOrderIdentifier(JSONArray jo) {
		JSONObject header = getHeader(jo);
		return (String) header.get(KEY_IDENTIFIER);
	}

	public PartnerDTO getDeliveryFromPartner(JSONArray jo) {
		return getDeliveryPartner(jo, PTC_PICKUP_ADDRESS);
	}

	private PartnerDTO getDeliveryPartner(JSONArray jo, String partnerTypeCode) {
		JSONObject data = getData(jo);
		JSONArray partners = (JSONArray) data.get("partner");
		Iterator<JSONObject> partnersIterator = partners.iterator();
		while(partnersIterator.hasNext()) {
			JSONObject partner = partnersIterator.next();
			if(partner.get("partnerTypeCode").equals(partnerTypeCode)) {
				return partnerDTOMapper.fromJsonPartner(partner);
			}
		}
		return null;
	}

	public Delivery getDelivery(JSONArray jo) {
		JSONObject data = getData(jo);
		JSONObject header = getHeader(jo);
		Delivery d = new Delivery();
		d.setCargoSpaceTypeCode((String)data.get("cargoSpaceTypeCode"));
		d.setCargoSpaceAmount(Integer.valueOf((String) data.get("cargoSpaceAmount")));
		d.setDeliveryDateTime(getTimeFromData(data, DTC_DELIVERY));
		d.setState(EDeliveryState.O);
		return d;
	}
	
	private LocalDateTime getTimeFromData(JSONObject data, String dtc) {
		JSONArray dates = (JSONArray) data.get("date");
		Iterator<JSONObject> datesIterator = dates.iterator();
		while(datesIterator.hasNext()) {
			JSONObject date = datesIterator.next();
			if(date.get("dateTypeCode").equals(dtc)) {
				return LocalDateTime.parse((String) date.get("dateTime"), DEF_DTFORMAT);
			}
		}
		return null;
	}

	private JSONObject getData(JSONArray jo) {
		return  (JSONObject) jo.get(INDEX_OF_DATA);
	}
	
	private JSONObject getHeader(JSONArray jo) {
		return  (JSONObject) jo.get(INDEX_OF_HEADER);
	}


}
