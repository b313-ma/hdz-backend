package at.jku.hdzb.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class PositionDTO {
	
	private Long id;
	
	private Integer amount;
	
	private String article;

}
