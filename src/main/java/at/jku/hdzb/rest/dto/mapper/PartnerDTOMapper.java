package at.jku.hdzb.rest.dto.mapper;

import java.util.stream.Collectors;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import at.jku.hdzb.HDZConfig;
import at.jku.hdzb.model.Partner;
import at.jku.hdzb.rest.dto.PartnerDTO;
import at.jku.hdzb.rest.dto.PartnerWithDelivieresDTO;
import at.jku.hdzb.service.DeliveryService;
import at.jku.hdzb.service.UserService;

@Component
public class PartnerDTOMapper {
	
	@Autowired
	DeliveryService deliveryService;
	
	@Autowired
	UserService userService;
	
	
	public static PartnerDTO mapToPartnerDTO(Partner e) {
		PartnerDTO dto = new PartnerDTO();
		dto.setId(e.getId());
		dto.setName(e.getName());
		dto.setStreet(e.getStreet());
		dto.setZip(e.getZip());
		dto.setCity(e.getCity());
		dto.setLatitude(e.getLatitude());
		dto.setLongitude(e.getLongitude());
		dto.setType(e.getType());
		dto.setIsSite(e.getIsSite());
		return dto;
	}
	
	public PartnerWithDelivieresDTO mapToPartnerWithDeliveriesDTOForUser(Partner e, Long userId) {
		PartnerWithDelivieresDTO dto = new PartnerWithDelivieresDTO();
		dto.setId(e.getId());
		dto.setName(e.getName());
		dto.setStreet(e.getStreet());
		dto.setZip(e.getZip());
		dto.setCity(e.getCity());
		dto.setLatitude(e.getLatitude());
		dto.setLongitude(e.getLongitude());
		if (userService.isAdmin(userId) || userService.isCustomer(userId)) {
			dto.setDeliveries(deliveryService.findAllByPartnerId(e.getId()).stream().map(DeliveryDTOMapper::mapToDeliveryDTO).collect(Collectors.toSet()));
		} else {
			dto.setDeliveries(deliveryService.findAllByPartnerIdForUser(e.getId(), userId).stream().map(DeliveryDTOMapper::mapToDeliveryDTO).collect(Collectors.toSet()));
		}
		
		dto.setNote(e.getNote());
		dto.setType(e.getType());
		dto.setIsSite(e.getIsSite());
		return dto;
	}
	
	public static PartnerDTO fromJsonPartner(JSONObject partner) {
		return new PartnerDTO(Long.decode((String) partner.get("identifier")),
				String.valueOf(partner.get("name1")),
				String.valueOf(partner.get("street")),
				Short.parseShort((String) partner.get("postalCode")),
				String.valueOf(partner.get("city")),
				String.valueOf(partner.get("countryCode")));
	}

	public static Partner toEntity(PartnerDTO dto)  {
		Partner p = new Partner();
		p.setIdentifier(dto.getIdentifier());
		p.setName(dto.getName());
		p.setStreet(dto.getStreet());
		p.setZip(dto.getZip());
		p.setCity(dto.getCity());
		p.setIsSite(dto.getIsSite());
		p.setLatitude(dto.getLatitude());
		p.setLongitude(dto.getLongitude());
		
		return p;
	}

}
