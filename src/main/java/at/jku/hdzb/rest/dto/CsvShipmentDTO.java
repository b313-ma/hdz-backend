package at.jku.hdzb.rest.dto;

import com.opencsv.bean.CsvBindByName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CsvShipmentDTO {
	
	@CsvBindByName(column = "Benutzergruppe")
//	@CsvBindByPosition(position = 0)
	private String benutzergruppe;
	
	@CsvBindByName(column = "Sendungsnummer")
	private String sendungsnummer;
	
	@CsvBindByName
	private String Ladedatum;
	
	@CsvBindByName(column = "Ladestelle")
	private String fromPartnerLadestelle;
	
	@CsvBindByName(column = "Ladestelle (Address)")
	private String fromPartnerAddress;
	
	@CsvBindByName(column = "Entladestelle")
	private String toPartnerName;
	
	@CsvBindByName(column = "Entladestelle (Address)")
	private String toPartnerAddress;
	
	@CsvBindByName(column = "Laderaum Type")
	private String cargospaceTypeCode;
	
	@CsvBindByName(column = "Gewicht der Sendung")
	private String cargospaceAmount;
		
}
