package at.jku.hdzb.rest.dto;

import java.util.Set;

import at.jku.hdzb.model.Delivery;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PartnerWithDelivieresDTO extends PartnerDTO {
	
	private Set<DeliveryDTO> deliveries;
	
	private String note;
	

}
