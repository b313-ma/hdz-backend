package at.jku.hdzb.rest.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import at.jku.hdzb.model.Delivery;
import at.jku.hdzb.model.enums.EDeliveryState;
import at.jku.hdzb.rest.dto.DeliveryDTO;
import at.jku.hdzb.service.DeliveryService;
import at.jku.hdzb.service.PartnerService;

@Component
public class DeliveryDTOMapper {
	
	@Autowired
	DeliveryService deliveryService;
	
	@Autowired
    PartnerService partnerService;
	
	public static DeliveryDTO mapToDeliveryDTO(Delivery d) {
		DeliveryDTO ret = new DeliveryDTO();
		ret.setId(d.getId());
		ret.setCargoSpaceAmount(d.getCargoSpaceAmount());
		ret.setCargoSpaceTypeCode(d.getCargoSpaceTypeCode());
		ret.setDeliveryFulfilled(d.getDeliveryDateTime());
		ret.setToSiteId(d.getTo().getId());
		ret.setFromSiteId(d.getFrom().getId());
		ret.setFromSiteName(d.getFrom().getName());
		ret.setToSiteName(d.getTo().getName());
		ret.setStatus(d.getState().getValue());
		ret.setDeliveryReturnRequested(d.getDeliveryReturnRequestedDateTime());
		ret.setAufschubRequested(d.getDeliveryAufschubRequestedDateTime());
		ret.setOrigSendungsnummer(d.getOrigSendungsnummer());
		//TODO falls man Artikel mitspeichern möchte
		ret.setPositions(null);
		
		return ret;
	}

	public Delivery mapToEntity(DeliveryDTO d) {
		Delivery ret = new Delivery();
		ret.setId(d.getId());
		ret.setCargoSpaceAmount(d.getCargoSpaceAmount());
		ret.setCargoSpaceTypeCode(d.getCargoSpaceTypeCode());
		ret.setDeliveryDateTime(d.getDeliveryFulfilled());
		ret.setTo(partnerService.findById(d.getToSiteId()));
		ret.setFrom(partnerService.findById(d.getFromSiteId()));
		ret.setState(EDeliveryState.fromAbbreviation(d.getStatus()));
		ret.setDeliveryReturnRequestedDateTime(d.getDeliveryReturnRequested());
		ret.setDeliveryAufschubRequestedDateTime(d.getAufschubRequested());
		ret.setOrigSendungsnummer(d.getOrigSendungsnummer());
		
		return ret;
	}

}
