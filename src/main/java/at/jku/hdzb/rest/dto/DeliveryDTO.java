package at.jku.hdzb.rest.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class DeliveryDTO implements Serializable {
	
	private Long id;
	
	private String origSendungsnummer;
	
	private Long toSiteId;
	
	private Long fromSiteId;
	
	private String fromSiteName;
	
	private String toSiteName;
	
	private String status;
	
	private LocalDateTime deliveryReturnRequested;
	
	private LocalDateTime aufschubRequested;
	
	private LocalDateTime deliveryFulfilled;
	
	private String cargoSpaceTypeCode;
	
	private Integer cargoSpaceAmount;
	
	private List<PositionDTO> positions;
	
}
