package at.jku.hdzb.rest.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class PartnerDTO {

	private Long id;
	
	private Long identifier;
	
	private String name;
	
	private String type;
	
	private String street;
	
	private short zip;
	
	private String city;
	
	private String countryCode;
	
	private BigDecimal latitude;
	
	private BigDecimal longitude;
	
	private Boolean isSite;

	public PartnerDTO(Long identifier, String name, String street, short zip, String city, String countryCode) {
		super();
		this.identifier = identifier;
		this.name = name;
		this.street = street;
		this.zip = zip;
		this.city = city;
		this.countryCode = countryCode;
	}
	
	
	
}
