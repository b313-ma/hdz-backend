package at.jku.hdzb;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import at.jku.hdzb.model.Delivery;
import at.jku.hdzb.model.Partner;
import at.jku.hdzb.model.User;
import at.jku.hdzb.model.enums.EDeliveryState;
import at.jku.hdzb.model.enums.ERole;
import at.jku.hdzb.repository.DeliveryRepository;
import at.jku.hdzb.repository.PartnerRepository;
import at.jku.hdzb.repository.UserRepository;

@SpringBootApplication
public class HdzBackendApplication {

	@Autowired
	PartnerRepository partnerRepository;
	
	@Autowired
	DeliveryRepository deliveryRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	HDZConfig config;
	
	public static void main(String[] args) {
		SpringApplication.run(HdzBackendApplication.class, args);
	}
	
	@EventListener(ApplicationReadyEvent.class)
	public void initializer() {
	    
	    if ( 1 == 0) {
			
		    User admin = User.builder().username("admin").role(ERole.ADMIN).build();
		    admin = userRepository.save(admin);
		    config.setAdminId(admin.getId());
		    
			
			if (partnerRepository.findAll().size() > 0) {
				System.out.println("----------------------- Bereits Daten vorhanden");
				return;
			}
		    System.out.println("-------------------------------Initializing data");
		    Partner leitl = Partner.builder().created(LocalDateTime.now()).identifier(1234L).isSite(true).latitude(new BigDecimal(1)).longitude(new BigDecimal(1)).city("Eferding")
		    		.zip(Short.parseShort("4070")).street("Leitlstraße 1").name("Bauhütte Leitl-Werke GmbH").note("Das Werk in Eferding").type("PA").build();
		    Partner privat = Partner.builder().created(LocalDateTime.now()).identifier(1232L).isSite(true).latitude(new BigDecimal(1)).longitude(new BigDecimal(1)).city("Thalheim b. Wels")
		    		.zip(Short.parseShort("4600")).street("Pilgerweg 15").name("Fam. Rodlberger").note("Privates Bauvorhaben. Geplante Fertigstellung Rohbau KW38").type("DA").build();
		    Partner bauhaus = Partner.builder().created(LocalDateTime.now()).identifier(1231L).isSite(true).latitude(new BigDecimal(1)).longitude(new BigDecimal(1)).city("Steyr")
		    		.zip(Short.parseShort("4400")).street("Ennser Straße 33c").name("BAUHAUS Steyr").note("Leihgebühr für Baustützen fällig").type("DA").build();
		    Partner kirchdorfer = Partner.builder().created(LocalDateTime.now()).identifier(1230L).isSite(true).latitude(new BigDecimal(1)).longitude(new BigDecimal(1)).city("Kirchdorf")
		    		.zip(Short.parseShort("4560")).street("Hofmannstraße 4").name("Kirchdorfer Zementwerk").note("Kirchdorfer Zementwerk Hofmann GesmbH").type("PA").build();
		    
		    partnerRepository.save(leitl);
		    partnerRepository.save(privat);
		    partnerRepository.save(bauhaus);
		    partnerRepository.save(kirchdorfer);
		    
		    String[] cargoTypes = {"EU-PAL", "BAUSTUETZE", "TONNE", "SILO-100", "SILO-50"};
		    
		    List<Delivery> deliveries = new ArrayList<>();
		    for (int i = 0; i < 40; i++) {
		    	deliveries.add(Delivery.builder().cargoSpaceAmount(ThreadLocalRandom.current().nextInt(1, 100))
		    			.cargoSpaceTypeCode(cargoTypes[i%5]).deliveryDateTime(LocalDateTime.now().minusDays(i*2))
		    			.from(leitl).to(privat).state(EDeliveryState.O).build());
		    }
		    for (int i = 0; i < 40; i++) {
		    	deliveries.add(Delivery.builder().cargoSpaceAmount(ThreadLocalRandom.current().nextInt(1, 100))
		    			.cargoSpaceTypeCode(cargoTypes[i%5]).deliveryDateTime(LocalDateTime.now().minusDays(i*2))
		    			.from(kirchdorfer).to(privat).state(EDeliveryState.A).build());
		    }
		    for (int i = 0; i < 40; i++) {
		    	deliveries.add(Delivery.builder().cargoSpaceAmount(ThreadLocalRandom.current().nextInt(1, 100))
		    			.cargoSpaceTypeCode(cargoTypes[i%5]).deliveryDateTime(LocalDateTime.now().minusDays(i*2))
		    			.from(kirchdorfer).to(bauhaus).state(EDeliveryState.O).build());
		    }
		    deliveryRepository.saveAll(deliveries);
		    
		    User bearbeiterKirchdorfer = User.builder().id(2L).username("Bearbeiter (Kirchdorfer)").role(ERole.INDUSTRIE).build();
		    bearbeiterKirchdorfer.setSources(Arrays.asList(kirchdorfer));
		    
		    User bearbeiterLeitl = User.builder().id(3L).username("Bearbeiter (Leitl)").role(ERole.INDUSTRIE).build();
		    bearbeiterLeitl.setSources(Arrays.asList(leitl));
		    
		    User heidlZT = User.builder().id(4L).username("Heidl Architekten ZT").role(ERole.KUNDE).build();
		    heidlZT.setTargets(Arrays.asList(privat, bauhaus));
		    
		    userRepository.saveAll(Arrays.asList(bearbeiterKirchdorfer, bearbeiterLeitl, heidlZT));
		    
		}
    }

}
