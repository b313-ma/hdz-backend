package at.jku.hdzb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import at.jku.hdzb.model.SyncHist;
import at.jku.hdzb.model.enums.ESyncStatus;

@Repository
public interface SyncHistRepository extends JpaRepository<SyncHist, Long> {
	
	List<SyncHist> findByOrderIdentifierAndStatus(String orderIdentifier, ESyncStatus status);

}
