package at.jku.hdzb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import at.jku.hdzb.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

	@Query(nativeQuery = true, value = "SELECT partner_source_id from users_sources us where user_id = :userId")
	List<Long> getSourceIdsForUser(@Param("userId") Long userId);

	@Query(nativeQuery = true, value = "SELECT partner_target_id from users_targets us where user_id = :userId")
	List<Long> getTargetIdsForUser(@Param("userId") Long userId);

}
