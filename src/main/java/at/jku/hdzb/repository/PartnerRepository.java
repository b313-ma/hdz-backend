package at.jku.hdzb.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import at.jku.hdzb.model.Partner;

@Repository
public interface PartnerRepository extends JpaRepository<Partner, Long> {

//	@Query("SELECT new at.jku.hdzb.rest.dto.PartnerDTO(p.id, p.name, s.type, s.street, s.zip, s.city, s.latitude, s.longitude) "
//			+ "FROM Partner p ")
//	List<PartnerDTO> findAllPartnerSites();
	List<Partner> findAllByIsSiteTrue();
	
	//TODO eventuell JPAModelEntityProcessor anwenden und nicht hardcoden
//	@EntityGraph(attributePaths = "deliveries")
	Partner findAllById(Long id);

	Optional<Partner> findByIdentifier(Long identifier);

	/**
	 * Findet alle Partner die die Sources (in der Regel das Unternehmen in dem er arbeteitet) des User als Ziel hat,
	 * oder dessen Konfiguration in users_targets bestimmte ziele vorgibt (in der Regel als Privatanwender, Architekt, etc.)
	 * @param userId
	 * @return
	 */
	@Query(nativeQuery = true, value = "SELECT distinct(p.*) FROM delivery d join partner p on d.to_partner_id = p.id "
			+ "where d.from_partner_id in (   "
			+ "SELECT us.partner_source_id FROM User u join USERS_SOURCES us on u.id = us.user_id "
			+ "where u.id = :userId)  "
			+ "UNION "
			+ "SELECT distinct(p.*) FROM partner p "
			+ "where p.id in (   "
			+ "SELECT ut.partner_target_id FROM User u join USERS_TARGETS ut on u.id = ut.user_id "
			+ "where u.id = :userId) ")
	List<Partner> findAllForUser(@Param("userId") Long userId);
	
	

}

