package at.jku.hdzb.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import at.jku.hdzb.model.Delivery;

@Repository
public interface DeliveryRepository extends JpaRepository<Delivery, Long> {
	
	@Query(nativeQuery = true,
			value = "SELECT * FROM Delivery d "
					+ "WHERE d.to_partner_id = :to_id "
					+ "and d.state != 'G' "
					+ "and d.from_partner_id in :from_ids "
					+ "order by d.delivery_date_time")
	public Set<Delivery> findAllByToIdAndFromIdsInOrderByDeliveryDateTimeAsc(@Param("to_id") Long toId, @Param("from_ids") List<Long> fromIds);

	@Query(nativeQuery = true,
			value = "SELECT * FROM Delivery d WHERE d.to_partner_id = :id and d.state != 'G' order by d.delivery_date_time")
	public Set<Delivery> findAllByToIdOrderByDeliveryDateTimeAsc(@Param("id") Long id);

	@Query(nativeQuery = true,
			value = "SELECT * FROM Delivery d "
					+ "WHERE d.state != 'G' "
					+ "and d.from_partner_id in :from_ids "
					+ "order by d.delivery_date_time")
	public List<Delivery> findAllFromIdsInOrderByDeliveryDateTimeAsc(@Param("from_ids") List<Long> fromIds);

	@Query(nativeQuery = true,
			value = "SELECT * FROM Delivery d "
					+ "WHERE d.state != 'G' "
					+ "and d.to_partner_id in :to_ids "
					+ "order by d.delivery_date_time")
	public List<Delivery> findAllToIdsInOrderByDeliveryDateTimeAsc(@Param("to_ids") List<Long> toIds);

}
