package at.jku.hdzb.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

import at.jku.hdzb.model.enums.ESyncStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
//@Table(uniqueConstraints={@UniqueConstraint(columnNames = {"orderIdentifier" , "status"})})
public class SyncHist {
	
	@Id
	@GeneratedValue
	private Long id;
	
	String orderIdentifier;
	
	@Enumerated(EnumType.STRING)
	ESyncStatus status;
	
	@CreationTimestamp
	LocalDateTime created;

}
