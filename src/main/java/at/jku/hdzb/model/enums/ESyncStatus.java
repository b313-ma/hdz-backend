package at.jku.hdzb.model.enums;

public enum ESyncStatus {

	S("success"), 
	F("failed"),
	P("pending");

	
	String value;
	
	ESyncStatus(String value) {
		this.value = value;
	}
	
	
	
}
