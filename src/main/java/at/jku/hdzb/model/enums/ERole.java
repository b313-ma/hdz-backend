package at.jku.hdzb.model.enums;

public enum ERole {
	
	INDUSTRIE("Industrie"), 
	KUNDE("Kunde"), 
	ADMIN("Admin");

	
	String value;
	
	ERole(String value) {
		this.value = value;
	}
	

}
