package at.jku.hdzb.model.enums;

import lombok.Getter;

public enum EDeliveryState {
	
	O("Vor Ort"), 
	R("Angefordert"),
	V("Angefordert (Aufschub)"),
	A("Abholbereit"),
	G("Abgeholt");

	@Getter
	String value;
	
	EDeliveryState(String value) {
		this.value = value;
	}
	
	public static EDeliveryState fromAbbreviation(String value) {
		switch(value) {
		case "Vor Ort": 
			return EDeliveryState.O;
		case "Angefordert": 
			return EDeliveryState.R;
		case "Abholbereit": 
			return EDeliveryState.A;
		case "Angefordert (Aufschub)":
			return EDeliveryState.V;
		default: 
			return EDeliveryState.G;
		}
	}

}
