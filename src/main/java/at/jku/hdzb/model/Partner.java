package at.jku.hdzb.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Builder.Default;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Partner {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * identifier seitens Satiamo
	 */
	@Column(unique = true)
	private Long identifier;
	
	private String name;
	
	private String type;
	
	private String street;
	
	private short zip;
	
	private String city;
	
	@Default
	@Column(precision = 9, scale = 7)
	private BigDecimal latitude = new BigDecimal(1);
	
	@Default
	@Column(precision = 9, scale = 7)
	private BigDecimal longitude = new BigDecimal(1);
	
	private String note;
	
	@Default
	private Boolean isSite = Boolean.FALSE;
	
	@CreationTimestamp
	private LocalDateTime created;
	
	@UpdateTimestamp
	private LocalDateTime edited;

	public Partner(Long identifier, String name, String street, short zip, String city, Boolean isSite) {
		super();
		this.identifier = identifier;
		this.name = name;
		this.street = street;
		this.zip = zip;
		this.city = city;
		this.isSite = isSite;
	}
	

}
