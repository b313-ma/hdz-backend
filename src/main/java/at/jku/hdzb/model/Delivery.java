package at.jku.hdzb.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import at.jku.hdzb.model.enums.EDeliveryState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
public class Delivery {

	@Id
	@GeneratedValue
	private Long id;
	
	private String origSendungsnummer;
	
	@OneToMany(mappedBy = "delivery")
	private List<Position> position;
	
	private String cargoSpaceTypeCode;
	
	private Integer cargoSpaceAmount;
	
	@Enumerated(EnumType.STRING)
	private EDeliveryState state;
	
	private LocalDateTime deliveryAufschubRequestedDateTime;
	
	private LocalDateTime deliveryReturnRequestedDateTime;
	
	private LocalDateTime deliveryReturnedDateTime;
	
	private LocalDateTime deliveryDateTime;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "from_partner_id", nullable = false)
	private Partner from;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "to_partner_id", nullable = false)
	private Partner to;
	
}
