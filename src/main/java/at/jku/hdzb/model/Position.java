package at.jku.hdzb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Position {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "delivery_id", nullable = false)
	private Delivery delivery;
	
	@ManyToOne
	private Article article;
	
	private Integer amount;
}
