package at.jku.hdzb.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.jku.hdzb.HDZConfig;
import at.jku.hdzb.model.User;
import at.jku.hdzb.model.enums.ERole;
import at.jku.hdzb.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository repository;
	
	@Autowired
	HDZConfig config;
	
	@Override
	public List<User> findAll() {
		return repository.findAll();
	}

	@Override
	public Optional<User> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public List<Long> getSourceIdsForUser(Long userId) {
		return repository.getSourceIdsForUser(userId);
	}
	
	@Override
	public List<Long> getTargetIdsForUser(Long userId) {
		return repository.getTargetIdsForUser(userId);
	}


	@Override
	public boolean isAdmin(Long userId) {
		return userId.equals(config.getAdminId());
	}

	@Override
	public boolean isCustomer(Long userId) {
		User u =  repository.findById(userId).orElseThrow(() -> new EntityNotFoundException("User mit ID " + userId + " existiert nicht."));
		return ERole.KUNDE.equals(u.getRole());
	}


}
