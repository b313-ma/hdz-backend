package at.jku.hdzb.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.jku.hdzb.model.SyncHist;
import at.jku.hdzb.model.enums.ESyncStatus;
import at.jku.hdzb.repository.SyncHistRepository;

@Service
public class SyncHistServiceImpl implements SyncHistService {
	
	@Autowired
	SyncHistRepository repository;
	
	@Override
	public void setSyncedWith(String identifier, ESyncStatus status) {
		repository.save(new SyncHist(null, identifier, status, LocalDateTime.now()));
	}

	@Override
	public List<SyncHist> findByIdentifierAndAlreadySynced(String orderIdentifier) {
		return repository.findByOrderIdentifierAndStatus(orderIdentifier, ESyncStatus.S);
	}

}
