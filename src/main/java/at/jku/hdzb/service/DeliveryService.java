package at.jku.hdzb.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import at.jku.hdzb.model.Delivery;

public interface DeliveryService {
	
	Delivery save(Delivery d);
	
	Set<Delivery> findAllByPartnerId(Long id);
	
	Set<Delivery> findAllByPartnerIdForUser(Long id, Long userId);

	Optional<Delivery> findById(Long id);
	
	Delivery updateStatus(Delivery delivery);

	List<Delivery> findAllForUser(Long userId);

	List<Delivery> findAll();



}
