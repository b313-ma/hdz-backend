package at.jku.hdzb.service;

import java.util.List;

import org.springframework.stereotype.Service;

import at.jku.hdzb.model.Partner;
import at.jku.hdzb.rest.dto.PartnerDTO;

@Service
public interface PartnerService {
	
	public List<Partner> findAll();
	
	public List<PartnerDTO> findAllMinimalDTO();

	public List<PartnerDTO> findAllMinimalDTOfor(Long userId);
	
	public Partner findById(Long id);

	public Partner saveOrUpdate(Partner p);
	
}
