package at.jku.hdzb.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.jku.hdzb.model.Delivery;
import at.jku.hdzb.repository.DeliveryRepository;

@Service
public class DeliveryServiceImpl implements DeliveryService {

	@Autowired
	DeliveryRepository repository;
	
	@Autowired
	UserService userService;
	
	@Override
	public Delivery save(Delivery d) {
		return repository.save(d);
	}
	
	@Override
	public Set<Delivery> findAllByPartnerId(Long id) {
		return repository.findAllByToIdOrderByDeliveryDateTimeAsc(id);
	}

	@Override
	public Set<Delivery> findAllByPartnerIdForUser(Long id, Long userId) {
		List<Long> fromIds = userService.getSourceIdsForUser(userId);
		return repository.findAllByToIdAndFromIdsInOrderByDeliveryDateTimeAsc(id, fromIds);
	}

	@Override
	public Optional<Delivery> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public Delivery updateStatus(Delivery delivery) {
		Delivery updDelivery = repository.findById(delivery.getId()).orElseThrow(() -> new EntityNotFoundException("No delivery found for id " + delivery.getId()));
		updDelivery.setState(delivery.getState());
		repository.save(updDelivery);
		return updDelivery;
	}

	@Override
	public List<Delivery> findAllForUser(Long userId) {
		List<Long> fromIds = userService.getSourceIdsForUser(userId);
		List<Long> toIds = userService.getTargetIdsForUser(userId);
		
		List<Delivery> combined = repository.findAllFromIdsInOrderByDeliveryDateTimeAsc(fromIds);
		combined.addAll(repository.findAllToIdsInOrderByDeliveryDateTimeAsc(toIds));
		return combined; 
				
	}

	@Override
	public List<Delivery> findAll() {
		return repository.findAll();
	}


}
