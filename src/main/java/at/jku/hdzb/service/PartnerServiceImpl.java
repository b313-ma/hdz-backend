package at.jku.hdzb.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.jku.hdzb.model.Partner;
import at.jku.hdzb.repository.PartnerRepository;
import at.jku.hdzb.rest.dto.PartnerDTO;
import at.jku.hdzb.rest.dto.mapper.PartnerDTOMapper;

@Service
public class PartnerServiceImpl implements PartnerService {
	
	@Autowired
	private PartnerRepository partnerRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PartnerDTOMapper partnerDTOMapper;

	@Override
	public List<Partner> findAll() {
		return partnerRepository.findAll();
	}

	@Override
	public List<PartnerDTO> findAllMinimalDTO() {
		return partnerRepository.findAllByIsSiteTrue().stream().map(PartnerDTOMapper::mapToPartnerDTO).collect(Collectors.toList());
	}

	@Override
	public Partner findById(Long id) {
		Optional<Partner> optS =  partnerRepository.findById(id);
		if (optS.isPresent()) {
			return optS.get();
		}
		return null;
	}

	@Override
	@Transactional
	public Partner saveOrUpdate(Partner p) {
		Optional<Partner> optP = partnerRepository.findByIdentifier(p.getIdentifier());
		if (optP.isPresent()) {
			p.setId(optP.get().getId());
			p.setCreated(optP.get().getCreated());
		}
		return partnerRepository.save(p);
	}

	@Override
	public List<PartnerDTO> findAllMinimalDTOfor(Long userId) {
		if (userService.isAdmin(userId)) {
			return partnerRepository.findAllByIsSiteTrue().stream().map(PartnerDTOMapper::mapToPartnerDTO).collect(Collectors.toList());
		}
		return partnerRepository.findAllForUser(userId).stream().map(PartnerDTOMapper::mapToPartnerDTO).collect(Collectors.toList());
	}

}
