package at.jku.hdzb.service;

import java.util.List;

import at.jku.hdzb.model.SyncHist;
import at.jku.hdzb.model.enums.ESyncStatus;

public interface SyncHistService {
	
	void setSyncedWith(String identifier, ESyncStatus status);

	List<SyncHist> findByIdentifierAndAlreadySynced(String orderIdentifier);

}
