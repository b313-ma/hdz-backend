package at.jku.hdzb.service;

import java.util.List;
import java.util.Optional;

import at.jku.hdzb.model.User;

public interface UserService {
	
	List<User> findAll();
	
	Optional<User> findById(Long id);

	List<Long> getSourceIdsForUser(Long userId);

	boolean isAdmin(Long userId);

	boolean isCustomer(Long userId);

	List<Long> getTargetIdsForUser(Long userId);

}
